# Specialklassens Ordbog

Velkommen til [specialklassens ordbog](https://specialklassensordbog.dk/).

Dette er en samling over nogle af de vigtigste ord, og deres definitioner, som bliver brugt i det daglige sprog i tegnestuen.

## Tilføj Et Ord

Hvis Mikkel ser dig værdig, kan du tilføje ord til ordbogen. Dette gør du ved at gå ind i `ordbog.xml` og tilføjer ordet som en `<entry>`.
Under `<entry>` skal `<word>` og `<definition>` laves og udfyldes. Har ordet en alternativ betydning, tilføj `<alt_definition>`.

For at gøre dit ordbogsopslag mere interessant, kan du bruge almindelige HTML-tags i teksten for `<definition>` og `<alt_definition>`.
Brug tags som `<strong>`, `<i>` og `<u>`.

Skal du linke til et andet ord i ordbogen, kan du bruge et `<a>` tag. Linket tager teksten inde i `<a>`-tagget som det den linker til.
Det er også muligt at definere en `href` attribut og så vil det blive brugt som link.
Smid et https link ind i href, hvis du vil linke til en ekstern side.

Exempler på links:

```xml
    <entry>
        <word>...</word>
        <definition> <!-- dette vil linke til ordet "duis" -->
            Deserunt culpa ea <a>duis</a> ullamco excepteur consectetur est excepteur ad amet qui in ut.
        </definition>
    </entry>
    <entry>
        <word>...</word>
        <definition> <!-- dette vil linke til ordet "ordbog" -->
            Deserunt culpa ea <a href="ordbog">duis</a> ullamco excepteur consectetur est excepteur ad amet qui in ut.
        </definition>
    </entry>
    <entry>
        <word>...</word>
        <definition> <!-- dette vil linke til hjemmesiden "google.com" -->
            Deserunt culpa ea <a href="https://www.google.com">duis</a> ullamco excepteur consectetur est excepteur ad amet qui in ut.
        </definition>
    </entry>
```
